package data;

import java.util.ArrayList;

/**
 * Class for the GroupChat object as saved in database
 */
public class GroupChat {
    private int groupChatID;
    private String groupChatName;
    private ArrayList<Message> messageList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();

    public GroupChat(){}

    public GroupChat(int ID,String name){
        this.groupChatID=ID;
        this.groupChatName=name;
    }

    public int getGroupChatID() {
        return groupChatID;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }


    public void setGroupChatName(String groupChatName) {
        this.groupChatName = groupChatName;
    }

    public void setMessageList(ArrayList<Message> messageList) {
       this.messageList=messageList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    public void setGroupChatId(int groupChatId) {
        this.groupChatID=groupChatId;
    }
}
